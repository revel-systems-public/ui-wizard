The task is to implement a **2 step UI wizard** to create a user account. There is no UX or UI constraints, this is up to you to decide.

The User information that we need to collect is described in the User type:

```
interface User {
  name: string;
  email: string;
  companyName: string;
  pricing: 'starter' | 'pro' | 'enterprise';
  billing: 'annualy' | 'monthly';
}
```

You can, for example collect the name and age in the first step and then email and newsletter in the second step. You may use a routing library such that every step is a separate route but this is completely optional and not required.

There is a dummy `sdk` package(implemented in the /sdk folder) which exports a `createUser` function. This function returns a `Promise`. Use it to simulate a request that creates a user account. Ex:

```
import { createUser } from 'sdk'

const details = {...}

createUser(details).then( ... )
```

The focus should be on code style and the way you approach the problem implementation wise. Feel free to use any other helper library although ideally the more code you write yourself the better.

### **Implementation requirements:**

- use either vanilla Javascript/TypeScript or React
- use npm to manage dependencies, there is pre-initialized package.json included in this repo

Feel free to use [create-react-app](https://github.com/facebookincubator/create-react-app) or webpack as a build tool