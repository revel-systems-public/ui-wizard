interface User {
    name: string;
    email: string;
    companyName: string;
    pricing: 'starter' | 'pro' | 'enterprise';
    billing: 'annualy' | 'monthly';
}

interface TokenResponse {
    user: User
    token: string
}

export const createUser = (user: User): Promise<TokenResponse> =>
    new Promise(resolve => {
        setTimeout(() => {
            resolve({ user, token: 'test.token' })
        }, 1000)
    })